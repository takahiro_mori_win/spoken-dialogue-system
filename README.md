# 音声対話システム

## 概要

botとの音声対話を行えるシステム。音声認識はGoogle Speech-to-Text API、応答文生成はA3RTのTalk APIを使用。ROS2連携がなされている。

## システム全体の起動

1. ros2インストール

2. A3RTとGoogle APIのKey取得と配置

3. sds-module内のAPIKey書き換えとGoogle API適用
```bash
# windows
set GOOGLE_APPLICATION_CREDENTIALS="[PATH]"
```

4. 音声対話システムモジュールインストール
```bash
cd sds-module
pip install . --user
cd .. #voice-dailogue-systemへ戻る
```

5. ros2のパスを通す
```bash
call C:\dev\ros2_foxy\local_setup.bat
```

6. VSのコマンドプロンプトでmsgフォルダ(interfaces)のビルド
```bash
colcon build --packages-select interfaces
call install\local_setup.bat
```

7. ros2モジュール(my_package)のビルド
```bash
colcon build --packages-select my_package
call install\local_setup.bat
```

8. 音声対話システムの起動
```bash
ros2 launch my_package sdsmod.launch.py  # 音声対話システムが全て同時起動
```

9. ダッシュボード起動(リポジトリ:dialouge-dashboardを入れる必要あり)
```bash
cd dialouge-dashbaord
cd server
npm install
npm run build
npm run start # サーバ起動
cd ..
npm install
npm run build
npm run serve # クライアント起動
```

10. (spoken-dialogue-systemにもどって)ダッシュボード用と管理ツール用のrosモジュール起動
```bash
ros2 run my_package dr # ダッシュボード連携
ros2 run my_package mm # 管理ツール用
```


## パッケージ構成

* (旧仕様)と書かれているものは前仕様のため無視して構わない

voice_dialogue: 音声対話システムの自作モジュール(旧仕様)
* device.py マイクのデバイス番号確認用
* streaming.py マイクストリーミング構築
* talk.py 応答文生成
* voicedialogue.py 音声対話システムのmain

sds-module/spoken_dialogue_system: ROSへのラッパーに対応した音声対話システムの自作モジュール（音声対話システムの構成を変える場合はこの中をいじる。詳しい仕様はこのパッケージ内のREADMEで）
* spokenInput.py 音声入力mod
* googleSpeechAPI.py 音声認識mod
* spokenAnalysis.py 音声解析mod
* spokenManager.py 音声管理mod:上記3つを合わせたもの(ROSで通信ができるようになれば不要になる)
* responseController.py 応答制御mod(応答タイミングの制御)
* naturalLanguageGnerator.py 応答文生成mod
* speachSynthesis.py 音声合成mod

src/my_package: ROSの通信パッケージ(ROS2のプログラムの記法やコマンドなどの詳しいことはこのパッケージ内のREADMEで)
* listener.py subscriberプログラム(旧仕様)
* publisher.py publisherプログラム(旧仕様)
* testModule.py テスト用(旧仕様)
* my_node.py 試作用(旧仕様)
* ros2_sm.py 音声管理ROS2ラッパー
* ros2_rc.py 応答制御ROS2ラッパー
* ros2_nlg.py 応答文生成ROS2ラッパー
* ros2_ss.py 音声合成ROS2ラッパー
* ros2_dr.py データ受信:ダッシュボードと連結


interfaces: ROS通信データのパラメータよう
* List.msg パラメータ

## 依存ライブラリ・環境

バージョン

* windows10
* python3 ver3.8.3
* ROS2-foxy 

python関係

* numpy
* pyaudio
* requests
* json

Google関連

* google
* gcloud
* google-auth
* google-cloud-speech
* grpc-google-cloud-speech-v1beta1

ROS2関連

* 多すぎるので省略(インストール項目記述)

## 音声対話システム　インストール・単体使用

Google Cloud Speech-to-Text APIの導入

1. Google Cloud Platformでアカウント作成
2. Google Speech-to-Text APIを有効に
3. 認証情報->認証情報を作成->サービスアカウントキー->jsonファイル作成
4. [Google Cloud SDK](https://cloud.google.com/sdk/docs/)をインストール
5. ライブラリのインストール

```bash
pip install install google gcloud google-auth google-cloud-speech grpc-google-cloud-speech-v1beta1
```

6. プロンプト起動のたびにパスを通す。([PATH]はcredential.jsonファイルのパス)

```bash
# mac
export GOOGLE_APPLICATION_CREDENTIALS="[PATH]"
# windows
set GOOGLE_APPLICATION_CREDENTIALS="[PATH]"
```

A3RTのTalk APIの導入

1. [A3RT](https://a3rt.recruit-tech.co.jp/product/talkAPI/)でAPI Key発行を押す
2. メールアドレスを登録してAPI Keyを発行
3. 内容について詳しくは[A3RT](https://a3rt.recruit-tech.co.jp/product/talkAPI/)のページで。

起動コマンド
```bash
cd voice_dialogue/voice_dialogue
python3 voicedialogue.py
```

モジュール化(ROS連携してimportする際に必須)
```bash
cd voice_dialogue
pip install . --user
```

## ROS2インストール・セットアップ(windows)

ROS2インストール・チュートリアルサイト:https://index.ros.org/doc/ros2/Installation/Foxy/

1. 上記サイトよりROS2-foxyをインストール(windowsの場合はC:\devに配置する)
2. 依存パッケージを全て入れる  (詳しくは:https://index.ros.org/doc/ros2/Installation/Rolling/Windows-Install-Binary/#rolling-windows-install-binary-installing-prerequisites)
    * Chocolatey (windows用のパッケージマネージャー)
    * OpenSSL Win64 OpenSSL v1.1.1g (PATHに必ず通す)
    * Visual Studio 2019 (C++パッケージのビルド時にVSのプロンプトを使用)
    * xmllint

    chocolatelyから

    * cmake
    * cppcheck

    gitリポジトリからダウンロード

    * asio.1.12.1.nupkg 
    * bullet.2.89.0.nupkg 
    * cunit.2.1.3.nupkg 
    * eigen-3.3.4.nupkg 
    * tinyxml-usestl.2.6.2.nupkg 
    * tinyxml2.6.0.0.nupkg 
    * log4cxx.0.10.0.nupkg
    
    python

    * setuptool
    * catkin_pkg 
    * cryptography 
    * EmPy 
    * ifcfg 
    * importlib-metadata 
    * lark-parser 
    * lxml 
    * pyparsing 
    * pyyaml
    * pytest 
    * pytest-mock 
    * coverage 
    * mock
    * flake8 
    * flake8-blind-except 
    * flake8-builtins 
    * flake8-class-newline 
    * flake8-comprehensions 
    * flake8-deprecated 
    * flake8-docstrings 
    * flake8-import-order 
    * flake8-quotes 
    * mypy 
    * pep8 
    * pydocstyle
3. ROS2コマンドを使用できるようにパスを通す(警告文が出るが無視して構わない)
```bash
call C:\dev\ros2_foxy\local_setup.bat
```
4. パッケージのビルド(c++のパッケージの場合VSのプロンプトで実行する)
```bash
cd voice-dialogue-system
colcon build --packages-select my_package
``` 

5. ビルドした後の環境設定
```bash
call install\local_setup.bat
```

6. ROS2(subscriber)起動
```bash
ros2 run my_package my_listener
```

7. もう一つプロンプトを開き、3,5の手順を行ったうえでROS2(publisher)起動。成功した場合、subscriber側プロンプトでデータ通信を確認できる。
```bash
ros2 run my_package pub
```

## msgファイルの更新

1. buildフォルダ内のmsgファイルを含むパッケージを削除する(今回はbuild/interfacesファイル)
2. VSのコマンドプロンプトで
```bash
colcon build --packages-select [msgファイルが含まれるパッケージ名]
call install\local_setup.bat
```

## msgファイルの追加

1. interfaces/msg(もしくはmsgファイルを格納するフォルダ)に*.msgを追加
2. CMakeList.txtの`rosidl_generate_interfaces`に追加したファイルpathを追記
3. VSのコマンドプロンプトで
```bash
colcon build --packages-select [msgファイルが含まれるパッケージ名]
call install\local_setup.bat
```

## Note

ストリーミング音声認識についての参考サイト: https://tech-blog.optim.co.jp/entry/2020/02/21/163000

ROS2のインデックス: https://index.ros.org/doc/ros2/

## Author

* 作成者: 森貴大

