import requests
import json
import sys

class NaturalLanguageGenerator:
    def __init__(self):
        self.key = "DZZRdCeR9xcoWQihTE9enw4RgMApQJxr"  # API Key
        self.url = "https://api.a3rt.recruit-tech.co.jp/talk/v1/smalltalk" # エンドポイントURL
        self.rc = { "word": "" }
        sys.stdout.write('NaturalLanguageGenerator start up.\n')
        sys.stdout.write('=====================================================\n')
        
    def response(self, query):
        try:
            if query == "dummy":
                return "はい"
            res = requests.post( self.url, {'apikey': self.key, 'query': query } )
            data = res.json()
            if data['status'] == 0:
                reply = data['results'][0]['reply'] # レスポンス結果
                return reply
            else:
                return "エラーです"
        except:
            return "例外的エラーです"