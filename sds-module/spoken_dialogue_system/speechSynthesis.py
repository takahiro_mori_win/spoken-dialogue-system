import sys
import win32com.client

class SpeechSynthesis:
    def __init__(self):
        self.voice = win32com.client.Dispatch("Sapi.spVoice")
        self.voice.Speak('') # dummy これがないと例外エラー吐く
        sys.stdout.write('SpeechSynthesis start up.\n')
        sys.stdout.write('=====================================================\n')

    def run(self, text):
        self.voice.Speak(text)