from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='my_package',
            executable='pub',
            output='screen'
        ),
        Node(
            package='my_package',
            executable='relay',
            output='screen'
        ),
        Node(
            package='my_package',
            executable='sub',
            output='screen'
        ),
    ])