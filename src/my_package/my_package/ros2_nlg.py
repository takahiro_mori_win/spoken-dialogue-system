import rclpy
import threading
import sys
from rclpy.node import Node
from interfaces.msg import Irc
from interfaces.msg import Inlg
from interfaces.msg import Imm
from spoken_dialogue_system.naturalLanguageGenerataor import NaturalLanguageGenerator

class RosNaturalLanguageGenerator(Node):
    def __init__(self, naturalLanguageGenerataor):
        super().__init__('natural_language_generator')
        self.naturalLanguageGenerataor = naturalLanguageGenerataor
        self.sub_rc = self.create_subscription(Irc, 'RCtoNLG', self.genrate, 1)
        self.pub_nlg = self.create_publisher(Inlg, 'NLGtoSS', 1)
        self.pub_nlg_dr = self.create_publisher(Inlg, 'NLGtoDR', 1)
        self.pub_mm = self.create_publisher(Imm, 'MM', 1)
        self.timer = self.create_timer(0.02, self.ping)
    
    def genrate(self, rc):
        query = rc.word
        if not query == "":
            reply = self.naturalLanguageGenerataor.response(query)
            nlg = Inlg()
            nlg.reply = reply
            print(nlg.reply)
            self.pub_nlg.publish(nlg)
            self.pub_nlg_dr.publish(nlg)

    def ping(self):
        mm = Imm()
        mm.mod = "nlg"
        self.pub_mm.publish(mm)


def runROS(pub):
    rclpy.spin(pub)

def shutdown():
    while True:
        key = input()
        if key == "kill":
            print("kill コマンド")
            sys.exit()

def main(args=None):
    nlg = NaturalLanguageGenerator()
    rclpy.init(args=args)
    rnlg = RosNaturalLanguageGenerator(nlg)

    ros = threading.Thread(target=runROS, args=(rnlg,))

    ros.setDaemon(True)

    ros.start()
    shutdown()

if __name__ == '__main__':
    main()