import rclpy
import threading
import sys
from rclpy.node import Node
from interfaces.msg import Iasr
from interfaces.msg import Isa
from interfaces.msg import Imm
from spoken_dialogue_system.spokenManager import SpokenManager

class RosSpokenManager(Node):
    def __init__(self, spokenManager):
        super().__init__('spoken_manager')
        self.spokenManager = spokenManager
        self.pub_asr = self.create_publisher(Iasr, 'ASRtoLU', 1)
        self.pub_sa = self.create_publisher(Isa, 'SAtoRC', 1)
        self.pub_asr_dr = self.create_publisher(Iasr, 'ASRtoDR', 1)
        self.pub_sa_dr = self.create_publisher(Isa, 'SAtoDR', 1)
        self.pub_mm = self.create_publisher(Imm, 'MM', 1)
        self.timer = self.create_timer(0.02, self.callback)

    def callback(self):
        asr = Iasr()
        asr.you = self.spokenManager.pubASR()['you']
        asr.is_final = self.spokenManager.pubASR()['is_final']
        print(asr.you, asr.is_final)
        self.pub_asr.publish(asr)
        self.pub_asr_dr.publish(asr)

        sa = Isa()
        sa.prevgrad = self.spokenManager.pubSA()['prevgrad']
        sa.frequency = self.spokenManager.pubSA()['frequency']
        sa.grad = self.spokenManager.pubSA()['grad']
        sa.power = self.spokenManager.pubSA()['power']
        sa.zerocross = self.spokenManager.pubSA()['zerocross']
        print(sa.prevgrad, sa.frequency, sa.grad, sa.power, sa.zerocross)
        self.pub_sa.publish(sa)
        self.pub_sa_dr.publish(sa)

        mm = Imm()
        mm.mod = "sm"
        self.pub_mm.publish(mm)

def runROS(pub):
    rclpy.spin(pub)

def runModule(spokenManager):
    spokenManager.run()

def shutdown():
    while True:
        key = input()
        if key == "kill":
            print("kill コマンド")
            sys.exit()

def main(args=None):
    sm = SpokenManager()
    rclpy.init(args=args)
    rsm = RosSpokenManager(sm)

    ros = threading.Thread(target=runROS, args=(rsm,))
    mod = threading.Thread(target=runModule, args=(sm,))

    ros.setDaemon(True)
    mod.setDaemon(True)

    ros.start()
    mod.start()
    shutdown()

if __name__ == '__main__':
    main()