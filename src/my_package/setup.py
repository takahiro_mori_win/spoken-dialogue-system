import os
from glob import glob
from setuptools import setup

package_name = 'my_package'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        ('share/' + package_name, glob('launch/*.launch.py')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='81802',
    maintainer_email='81802@todo.todo',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'pub = my_package.publisher:main',
            'relay = my_package.listener:main',
            'sub = my_package.testModule:main',
            'sm = my_package.ros2_sm:main',
            'lu = my_package.ros2_lu:main',
            'dm = my_package.ros2_dm:main',
            'rc = my_package.ros2_rc:main',
            'nlg = my_package.ros2_nlg:main',
            'ss = my_package.ros2_ss:main',
            'dr = my_package.ros2_dr:main',
            'mm = my_package.ros2_mm:main'
        ],
    },
)
