import requests
import json

class TalkAPI:
    """
    A3RTのTalk API
    """
    def __init__( self ):
        self.key = "DZZRdCeR9xcoWQihTE9enw4RgMApQJxr"  # API Key
        self.url = "https://api.a3rt.recruit-tech.co.jp/talk/v1/smalltalk" # エンドポイントURL
        
    def response( self, query ):
        """
        POSTリクエストによって得られた応答文を返す

        Arguments:
            query {str} -- 入力文

        Returns:
            str -- 応答文
        """
        try: 
            res = requests.post( self.url, {'apikey': self.key, 'query': query } )
            data = res.json() # jsonデータ
            if data['status'] == 0:
                reply = data['results'][0]['reply'] # レスポンス結果
                return reply
            else: # レスポンスに不備あり
                return "リクエストエラー { status: " + str(data['status'])+", message: " + data['message'] + "}"
        except:
            return "不明なエラー"