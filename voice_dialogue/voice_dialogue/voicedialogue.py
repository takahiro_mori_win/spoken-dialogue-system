#!/usr/bin/env python

# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#参考: https://tech-blog.optim.co.jp/entry/2020/02/21/163000

#export GOOGLE_APPLICATION_CREDENTIALS="credential.json"
#or
#set GOOGLE_APPLICATION_CREDENTIALS=C:\Users\81802\voice-dialogue\credential.json

#python3 ***.py

import time
import re
import sys
import numpy as np
import wave
import pyaudio
from google.cloud import speech_v1p1beta1 as speech
from voice_dialogue.talk import TalkAPI
from voice_dialogue.streaming import ResumableMicrophoneStream
from voice_dialogue.voice import Voice
from voice_dialogue.analysis import Analysis
from voice_dialogue.reaction import Reaction

# Audio recording parameters
SHUTDOWN_TIME = 20000
SAMPLE_RATE = 44100
CHUNK_SIZE = 2**10
iDEVICE = 1 #マイクデバイスのインデックス番号

#RED = '\033[0;31m'
#GREEN = '\033[0;32m'
#YELLOW = '\033[0;33m'
class VoiceDialogue():
    def __init__(self):
        self.word = ""
        self.is_final = False
        self.bot = ""
        self.turn = 0
        self.frequency = 0.0

        self.client = speech.SpeechClient()
        self.config = speech.types.RecognitionConfig(
            encoding=speech.enums.RecognitionConfig.AudioEncoding.LINEAR16,
            sample_rate_hertz=SAMPLE_RATE,
            language_code='ja-JP',
            max_alternatives=1)
        self.streaming_config = speech.types.StreamingRecognitionConfig(
            config=self.config,
            interim_results=True)
        self.mic_manager = ResumableMicrophoneStream(SAMPLE_RATE, CHUNK_SIZE, iDEVICE)
        self.talkapi = TalkAPI()
        self.voice = Voice()
        self.reaction = Reaction()
        #sys.stdout.write(YELLOW)
        sys.stdout.write('Talk API start.\n')
        sys.stdout.write('=====================================================\n')

    def get_current_time(self):
        """
        現在の時間を返す

        Returns:
            int -- 現在の時間
        """
        return int(round(time.time() * 1000))

    def listen_print_loop(self, responses, stream):
        """
        音声認識結果
        """
        for response in responses:
            """
            if self.get_current_time() - stream.start_time > SHUTDOWN_TIME:
                sys.stdout.write("Shutdown...\n")
                stream.closed = True
                break
            """

            if not response.results:
                continue

            result = response.results[0]
            if not result.alternatives:
                continue

            transcript = result.alternatives[0].transcript
            self.word = str(transcript)
            self.is_final = result.is_final
            #print(self.word, self.is_final)

    def runAnswer(self):
        prev = ""
        carry = ""
        while not self.mic_manager.closed:
            if self.is_final and prev != self.word:
                sys.stdout.write('YOU : ' + carry+self.word + '\n')
                if len(carry+self.word) < 6:
                    reaction = self.reaction.create("dummy")
                    carry = carry + self.word
                else:
                    reaction = self.talkapi.response(carry+self.word)
                    carry = ""

                sys.stdout.write('BOT : ' + reaction + '\n')
                self.voice.run(reaction)

                self.turn = 1
                self.bot = reaction
                prev = self.word
                """
                if prev != self.word:
                    sys.stdout.write('YOU : ' + carry + self.word + '\n')

                    if re.search(r'\b(終わり|終了)\b', self.word, re.I):
                        sys.stdout.write('Exiting...\n')
                        self.mic_manager.closed = True
                        break
                
                    ans = self.talkapi.response(self.word)
                    sys.stdout.write('BOT : ' + ans + '\n')
                    self.voice.run(ans)
                    self.bot = ans
                    carry = ""
                    self.turn = 1
                    prev = self.word
                elif not carryLock and react == self.word:
                    carry = carry + self.word
                    carryLock = True
                """

            if abs(self.mic_manager.prevgrad) > 1.0 and self.mic_manager.zerocross < 100 and prev != self.word:
                self.turn = 1
                sys.stdout.write('>YOU : ' + self.word + '\n')
                reaction = self.talkapi.response(self.word) if len(self.word) > 3 else self.reaction.create("dummy")
                if len(self.word) > 3:
                    carry = ""
                sys.stdout.write('>BOT : ' + reaction + '\n')
                self.voice.run(reaction)

                self.turn = 1
                self.bot = reaction
                prev = self.word
            else:
                self.turn = 0
            #print(self.word, self.is_final)


    def runRecognition(self):
        with self.mic_manager as stream:
            while not stream.closed:
                stream.audio_input = []
                #ストリーミング部分
                audio_generator = stream.generator()

                requests = (speech.types.StreamingRecognizeRequest(
                    audio_content=content)for content in audio_generator)

                #音声認識部分
                responses = self.client.streaming_recognize(self.streaming_config,
                                                   requests)
                # 音声処理部分
                self.listen_print_loop(responses, stream)

                """
                if stream.result_end_time > 0:
                    stream.final_request_end_time = stream.is_final_end_time
                    stream.result_end_time = 0
                    stream.last_audio_input = []
                    stream.last_audio_input = stream.audio_input
                    stream.audio_input = []
                    stream.restart_counter = stream.restart_counter + 1

                if not stream.last_transcript_was_final:
                    sys.stdout.write('\n')
                stream.new_stream = True
                """
    
    def log(self):
        return { "turn":self.turn, "you":self.word, "bot":self.bot, "frequency":self.mic_manager.frequency, "grad":self.mic_manager.grad, "power":self.mic_manager.power }

def main():
    # 単体用<今は使えない>
    voicedialogue = VoiceDialogue()
    voicedialogue.runRecognition()

if __name__ == '__main__':
    main()